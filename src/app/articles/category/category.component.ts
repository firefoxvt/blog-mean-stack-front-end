import { Component, OnInit, OnDestroy, Inject, Injector } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Article } from '../../models/article';
import { Category } from '../../models/category';
import { ArticleService } from '../../services/article.service';
import { Observable } from 'rxjs/Observable';
import { ConfigHost } from '../../services/_config';

@Component({
	selector: 'app-category',
	templateUrl: './category.component.html',
	styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit, OnDestroy {

	articles: Article[];

	category: Category;

	page: string;

	id: string;

	sub: any;

	sub2: any;

	num_page: any;

	configHost: ConfigHost;
	activatedRoute: ActivatedRoute;
	articleService: ArticleService;
	window: Window;

	constructor( private injector: Injector) {
		setTimeout(() => this.configHost = this.injector.get(ConfigHost));
		setTimeout(() => this.activatedRoute = this.injector.get(ActivatedRoute));
		setTimeout(() => this.articleService = this.injector.get(ArticleService));
		setTimeout(() => this.window = this.injector.get(Window));
	}

	ngOnInit() {
		this.sub2 = this.activatedRoute.params.subscribe(params => {
	       this.num_page = +params['num_page'] || 0;
	    });

		this.page = this.window.location.pathname.split('/')[1] || 'NguyenAnhQuoc';
		this.sub = this.activatedRoute.data.subscribe(res => {
			res = res.res;
			this.articles = res.articles;
			this.category = res.category;
		});
	}

	ngOnDestroy(){
		this.sub.unsubscribe();
	}

	srcImage(image){
		if(image != undefined && image != null && image != "")
			return this.configHost.apiUrl + "uploads/" + image;
		return "/assets/images/image-not-found.png";
	}
}
