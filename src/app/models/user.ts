export class User {
	constructor(
		public _id:string,
		public name:string,
		public email:string,
		public password:string,
		public phone:string,
		public address:string,
		public gender:string,
		public born:number,
		public image:string,
		public role:string,
		public page:string,
		public page_title: string, 
		public page_slogan: String,
		public created_on:number,
		public updated_on:number
		) { }
}
