import { Injectable, Input } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from "@angular/http";
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { ConfigHost } from './_config';

@Injectable()
export class AuthService extends ConfigHost {

	// apiUrl = 'https://secret-brook-24730.herokuapp.com/login/';

	@Input()
	apiUrl = this.apiUrl + 'login/';

	public configObservable = new Subject<string>();

  	constructor(private http: Http) {
  		super();
  	}

  	emitConfig(val) {
	    this.configObservable.next(val);
	}

  	login(auth: any){
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });

		return this.http.post(this.apiUrl, JSON.stringify(auth), options)
			.map((response: Response)=> {
				let obj = response.json();
				if (obj.user) {
					localStorage.setItem('currentUser', JSON.stringify(obj.user[0]));
				}
				return response.json();
			});
	}

	isLogin(auth: any){
		let headers = new Headers({ 'Content-Type': 'application/json' });
		let options = new RequestOptions({ headers: headers });

		return this.http.post(this.apiUrl + '/is-login', JSON.stringify(auth), options)
			.map((response: Response)=> {
				let obj = response.json();
				if (obj.user) {
					localStorage.setItem('currentUser', JSON.stringify(obj.user[0]));
				}
				return response.json();
			});
	}

	logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
    }
}