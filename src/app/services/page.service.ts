import { Http, Response } from "@angular/http";
import { Injectable, Input } from '@angular/core';

import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';

import { Page } from '../models/page';
import { ConfigHost } from './_config';

@Injectable()
export class PageService extends ConfigHost {

	// apiUrl = 'https://secret-brook-24730.herokuapp.com/page/';
	@Input()
	apiUrl = this.apiUrl + 'page/';

	constructor(private http: Http) {
		super();
	}

	getPage(page: string, page_type: string) {
		return this.http.get(this.apiUrl + page + "/" + page_type)
			.map(response => response.json());
	}
}
