import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
	})
export class DashboardComponent implements OnInit {

	user: object;
	constructor(private router: Router,
		private authService: AuthService) {
		this.user = JSON.parse(localStorage.getItem('currentUser'));
		if (this.user == null){
			this.router.navigate(['/']);
		} else {
			this.authService.isLogin( { email: this.user["email"], password: this.user["password"] })
				.subscribe( response => {
					if (response.status != 'success'){
						localStorage.removeItem("currentUser");
						this.router.navigate(['/']);
					}
				});
		}
	}

	ngOnInit() {
		this.user = JSON.parse(localStorage.getItem('currentUser'));
	}
}
